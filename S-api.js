// S-api.js

module.exports = function api(options) {
    let valid_ops = { sum: 'sum', product: 'product' }
  
    this.add('role:api,path:calculate', function(msg, respond) {
      let operation = msg.args.params.operation
      let left = msg.args.query.left
      let right = msg.args.query.right
      let integer = msg.args.query.integer
      if (integer == null) {
          integer = false
      }
  
      console.log(`api:calculate op:${operation} left:${left} right:${right}`)
  
      this.act('role:math', {
        cmd: valid_ops[operation],
        left: left,
        right: right,
        integer: integer,
      }, respond)
    })
  
    this.add('init:api', function(msg, respond) {
      console.log(`init:api msg:${msg}`)
      
      // endpoint will be:
      // http://localhost:3000/api/calculate/product?left=10&right=20
      this.act('role:web', {
        routes: {
          prefix: '/api',
          pin: 'role:api,path:*',
          map: {
            calculate: { GET: true, suffix: '/:operation' }
          }
        }
      }, respond)
    })
  }
  