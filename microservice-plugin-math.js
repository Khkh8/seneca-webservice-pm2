// microservice-plugin-math.js

module.exports = function math(options) {
    // seneca.add
    // - pattern: property pattern to match
    // - action: function to be called when property match
    this.add('role:math,cmd:sum',(msg, reply) => {
        let sum = msg.left + msg.right;
        reply(null, {answer: sum});
    });

    this.add('role:math,cmd:sum,integer:true',(msg, reply) => {
        let sum = Math.floor(msg.left) + Math.floor(msg.right);
        reply(null, {answer: sum});
    });

    this.add('role:math,cmd:product',(msg, reply) => {
        let result = msg.left * msg.right;
        reply(null, {answer: result});
    });
    
    this.wrap('role:math', function(msg, reply) {
        msg.left  = Number(msg.left).valueOf();
        msg.right = Number(msg.right).valueOf();
        this.prior(msg, reply);
    })
}