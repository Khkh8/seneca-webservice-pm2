// microservice.js

console.log('Starting microservice using tcp on port 10201')

require('seneca')()
    .use(require('./microservice-plugin-math'))
    .listen({ type: 'tcp', port: 10201 })

